package ru.eka.player;

/**
 * Класс для угадывания числа игроком
 *
 * @author Куцкая Э.А.,15ит18
 */
public class Player {
    int number = 0;

    /**
     * Метод генерирует число
     */
    public void guess() {
        number = (int) (Math.random() * 10);
    }
}
