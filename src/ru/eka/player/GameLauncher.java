package ru.eka.player;

/**
 * Класс запускающий игру.
 *
 * @author Куцкая Э.А.,15ит18
 */
public class GameLauncher {
    public static void main (String[] args) {
        GuessGame game = new GuessGame();
        game.startGame();
    }
}
