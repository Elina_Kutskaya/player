package ru.eka.player;

/**
 * Класс для представления игры
 *
 * @author Куцкая Э.А.,15ит18
 */
public class GuessGame {

    Player player1 = new Player();
    Player player2 = new Player();
    Player player3 = new Player();

    int targetNumber = (int) (Math.random() * 10);

    /**
     * Метод определяет игрока угадавшего число
     */
    public void startGame() {
        System.out.println("Я думаю, загаданное число - любое от 0 до 9...");
        System.out.println("Загаданное число - " + targetNumber);

        player1.guess();
        player2.guess();
        player3.guess();

        System.out.println("Первый игрок думает, что это число " + player1.number);
        System.out.println("Второй игрок думает, что это число " + player2.number);
        System.out.println("Третий игрок думает, что это число " + player3.number);

        if (isNumberGuess(player1) || isNumberGuess(player2) || isNumberGuess(player3)) {
            System.out.println("У нас есть победитель!");
            System.out.println("Первый игрок угадал? - " + isNumberGuess(player1));
            System.out.println("Второй игрок угадал? -  " + isNumberGuess(player2));
            System.out.println("Или третий игрок угадал? - " + isNumberGuess(player3));
            System.out.println("Конец игры");
        } else {
            System.out.println("Печально, победителя -НЕТ. Повторите попытку - сыграйте снова.");
        }
    }

    /**
     * Метод для сравнения числа игрока и числа для угадывания
     *
     * @param player
     * @return true
     */
    private boolean isNumberGuess(Player player) {
        boolean isWinner = false;
        if (player.number == targetNumber) {
            isWinner = true;
        }
        return isWinner;
    }
}
